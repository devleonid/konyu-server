from rest_framework import pagination, generics

class ProductsPagination(pagination.PageNumberPagination):
	page_size = 20