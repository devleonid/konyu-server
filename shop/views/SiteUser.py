from shop.models import SiteUser
from shop.serializers import SiteUserSerializer
from rest_framework import generics

class SiteUserList(generics.ListCreateAPIView):
	queryset = SiteUser.objects.all()
	serializer_class = SiteUserSerializer


class SiteUserDetail(generics.RetrieveUpdateDestroyAPIView):
	queryset = SiteUser.objects.all()
	serializer_class = SiteUserSerializer
