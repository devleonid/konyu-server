from django.db import models
from konyu.models import BaseModel
from django.contrib.auth.models import AbstractUser

class Product(BaseModel):
    name = models.CharField(max_length=100)
    description = models.TextField(max_length=300, blank=True, null=True)
    price = models.DecimalField(max_digits=10, decimal_places=3)
    seller = models.ForeignKey(
        "SiteUser",
        on_delete = models.CASCADE
    )

    class Meta:
        ordering = ["-modified_on","-created_on"]
        verbose_name = "Product"
        verbose_name_plural = "Products"

    def __str__(self):
        return self.name


class SiteUser(AbstractUser):
    background = models.TextField(max_length=500)
    certified = models.BooleanField(default=False)
    
    class Meta:
        ordering = ["is_active","username","last_name","first_name"]
        verbose_name = "SiteUser"
        verbose_name_plural = "SiteUsers"

    def __str__(self):
        return self.get_full_name()



class ProductRating(BaseModel):
    rater = models.ForeignKey(
        "SiteUser",
        on_delete=models.CASCADE
    )
    rating = models.FloatField()
    product = models.ForeignKey(
        "Product",
        on_delete = models.CASCADE
    )
    comment = models.TextField(max_length=500, null=True, blank=True)

    class Meta:
        ordering = ['rater__is_active','-modified_on','-created_on']
        verbose_name = "ProductRating"
        verbose_name_plural = "ProductRatings"

    def get_shortened_comment(self):
        return "{}...".format(self.comment[:30]) if len(self.comment) > 30 else self.comment