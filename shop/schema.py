from graphene import relay
from graphene_django import DjangoObjectType
from shop.models import (Product as ProductModel, 
	SiteUser as SiteUserModel, 
	ProductRating as ProductRatingModel)

class SiteUser(DjangoObjectType):
    class Meta:
        model = SiteUserModel

class Product(DjangoObjectType):
	class Meta:
		model = ProductModel
		filter_fields = {
			'name':['exact','icontains','istartswith'],
			'price':['exact','lt','gt']
		}
		interfaces = (relay.Node, )

class ProductRating(DjangoObjectType):
	class Meta:
		model = ProductRatingModel