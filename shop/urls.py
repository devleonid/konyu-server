from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns
from django.conf.urls import url

from . import views
urlpatterns = [
    url(r'^api/v1/products/$', views.Product.ProductList.as_view()),
    url(r'^api/v1/product/(?P<pk>[0-9]+)/$', views.Product.ProductDetail.as_view()),
    url(r'^api/v1/siteusers/$', views.SiteUser.SiteUserList.as_view()),
    url(r'^api/v1/siteuser/(?P<pk>[0-9]+)/$', views.SiteUser.SiteUserDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
