from django.contrib import admin
from shop.models import SiteUser, Product, ProductRating

@admin.register(SiteUser)
class SiteUserAdmin(admin.ModelAdmin):
    pass

@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    pass

@admin.register(ProductRating)
class ProductRatingAdmin(admin.ModelAdmin):
    pass

