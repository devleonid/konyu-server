from rest_framework import serializers
from shop.models import Product, SiteUser, ProductRating

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('name','description','price','seller')

class SiteUserSerializer(serializers.ModelSerializer):
	class Meta:
		model = SiteUser
		fields = '__all__'

class ProductRatingSerializer(serializers.ModelSerializer):
	class Meta:
		model = ProductRating
		fields = '__all__'