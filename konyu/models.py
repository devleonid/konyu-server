from django.db import models
from django.utils import timezone

class BaseModel(models.Model):

    created_on = models.DateTimeField()
    modified_on = models.DateTimeField()

    class Meta:
        abstract=True

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_on = timezone.now()
            self.modified_on = timezone.now()
        else:
            self.modified_on = timezone.now()
        return super(BaseModel, self).save(*args, **kwargs)
