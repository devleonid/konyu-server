import graphene
from shop.schema import SiteUser, Product, ProductRating
from graphene_django import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField
from shop.models import (Product as ProductModel, 
	SiteUser as SiteUserModel, 
	ProductRating as ProductRatingModel)

class Query(graphene.ObjectType):
    users = graphene.List(SiteUser)
    products = DjangoFilterConnectionField(Product)
    
    def resolve_users(self, info):
        return SiteUserModel.objects.all()

schema = graphene.Schema(query=Query)